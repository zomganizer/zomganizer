/*
    Zomganizer: an HTML5 organizer with offline-mode and other features
    Copyright (C) 2010  Peter Zotov <whitequark@whitequark.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

Date.CultureInfo.firstDayOfWeek = 1;
Date.CultureInfo.dateElementOrder = 'dmy';
Date.CultureInfo.formatPatterns = {
  shortDate: "dd.MM.yyyy",
  longDate: "d MMMM yyyy",
  shortTime: "H:mm",
  longTime: "H:mm:ss",
  fullDateTime: "d MMMM yyyy H:mm:ss",
  sortableDateTime: "yyyy-MM-ddTHH:mm:ss",
  universalSortableDateTime: "yyyy-MM-dd HH:mm:ssZ",
  rfc1123: "ddd, dd MMM yyyy HH:mm:ss GMT",
  monthDay: "MMMM dd",
  yearMonth: "MMMM yyyy"
};

var zomg = {
  date_format: Date.CultureInfo.formatPatterns.shortDate,

  date_picker_options: {
    constrainInput: true,
    showWeek: true,
    showOtherMonths: true,
    dateFormat: 'dd.mm.yy',
    firstDay: Date.CultureInfo.firstDayOfWeek,
  },

  days_in_month: function(month) {
    var date = new Date(new Date().getYear(), month);
    var days;
    while(date.getMonth() == month) {
      days = date.getDate();
      date.setDate(date.getDate() + 1);
    }
    return days;
  },

  filters: {},

  popup_active: false,

  show_popup: function(no_close) {
    if(no_close) {
      $('#popup .close-button').hide();
    }
    $('#popup').animate({ opacity: 'show' }, 'fast', 'linear');
    zomg.select_helper();
    zomg.popup_active = true;
  },

  hide_popup: function() {
    $('#popup').animate({ opacity: 'hide' }, 'fast', 'linear');
    $('#popup .close-button').show();
    zomg.popup_active = false;
  },

  show_errors: function(form, errors) {
    var error_info = $(form).find('.errors');
    error_info.empty();

    $(form).find('[data-attribute]').removeClass('invalid');
    $A(errors).each(function(error) {
      error_info.append(error.message + '<br/>');
      $(form).find('[data-attribute='+error.field+']').addClass('invalid');
    });

    error_info.show();
  },

  prepare_filters: function() {
    $('nav.filter').each(function() {
      var nav = this;
      var list = $('#'+nav.id.replace('-filter', ''));

      var update_filters = function() {
        var status = zomg.filters[nav.id] || '';
        $(nav).children().each(function() {
          var curr = $(this).attr('data-filter');
          if(curr == status) {
            $(this).addClass('filter-active');
            if(status != '')
              list.addClass('filter-'+curr);
          } else {
            $(this).removeClass('filter-active');
            list.removeClass('filter-'+curr);
          }
        });
      };

      $(nav).children().each(function() {
        $(this).bind('click', function(event) {
          var new_filter = $(this).attr('data-filter');
          zomg.filters[nav.id] = new_filter;

          update_filters();
          event.preventDefault();
        });
      });

      update_filters();
    });
  },

  prepare_lists: sortListsInitially,

  prepare: function() {
    zomg.prepare_filters();
    zomg.prepare_lists();
  },

  refresh: function() {
    document.location.hash = '';
    if(zomg.current_view)
      zomg.current_view(null, true);
  },

  check_sync: function() {
    var objects = [ Task.all(), Category.all() ];
    var desynced = false;
    $A(objects).flatten().each(function(obj) {
      if(!obj.synced) desynced = true;
    });

    $('.sync-status').hide();
    if(desynced) {
      $('.sync-status.desync').show();
    } else {
      $('.sync-status.sync').show();
    }

    return desynced;
  },

  select_helper: function() {
    $('select option').each(function() {
      if(this.value == $(this).parent('select').attr('data-value')) {
        this.selected = true;
      }
    });
  }
};

if(!(typeof localStorage === 'object')) {
  window.localStorage = {};
}

$(function() {
  zomg.check_sync();

  $('#popup .close-button').bind('click', zomg.hide_popup);

  $('input[class="date"]').live('click', function(event) {
    $(event.currentTarget).datepicker(zomg.date_picker_options);
    $(event.currentTarget).datepicker('show');
  });
  $('input[class="submit"]').live('mousedown', function(event) {
    $('input[class="date"]').datepicker('destroy');
  });

  $('div#priority-bar span').live('click', function(event) {
    var level = $(this).attr('class').replace('prio-', '');
    var input = $('div#priority-bar input')[0];
    var textarea = $('div#priority-bar').parent().find('textarea')[0];

    textarea.className = 'prio-' + level;
    input.value = level;
  });

  (function() {
    var lastUpdate;
    function updateDate() {
      var now = Date.today().toString(zomg.date_format);
      if(now != lastUpdate) {
        $('#bar .date')[0].textContent = now;
        lastUpdate = now;

        if(zomg.current_view == TasksController.dashboard)
          zomg.current_view(null, true);
      }
      setTimeout(updateDate, 60000);
    }
    updateDate();
  })();
});
