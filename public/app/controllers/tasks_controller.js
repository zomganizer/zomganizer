/*
    Zomganizer: an HTML5 organizer with offline-mode and other features
    Copyright (C) 2010  Peter Zotov <whitequark@whitequark.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

o_O('TasksController', {
  dashboard: function() {
    var all_tasks = Task.all().map(Task.filter).compact().map(Task.prepare);
    var data = {};
    $A(['missed', 'today', 'tomorrow', 'week']).each(function(tt) {
      data[tt+'_tasks'] = all_tasks.select(function(t){ return t.schedule && t.schedule.indexOf(tt) != -1; });
    });

    o_O.render('tasks/dashboard', data, {set: 'div#yield',
          callback: zomg.prepare}, ['tasks/_task', 'tasks/_task_header']);
    zomg.current_view = TasksController.dashboard;
  },

  list: function() {
    o_O.render('tasks/list', { tasks: Task.all().map(Task.prepare) },
           {set: 'div#yield', callback: zomg.prepare},
           ['tasks/_task', 'tasks/_task_header']);
    zomg.current_view = TasksController.list;
  },

  show: function(params) {
    var task = Task.find(params['id']);
    o_O.render('tasks/show', Task.prepare(task),
        {set: 'div#popup-contents', callback: zomg.show_popup}, ['tasks/show']);
  },

  _setup_form: function() {
    var categories = Category.all().map(Category.filter).compact();
    $('input#task-category').autocomplete({
      source: categories.map(function(c) { return c.name; }),
      select: function(event, ui) {
        $A(categories).each(function(c) {
          if(c.name == ui.item.value && c.day_of_week > 0) {
            var date = Date.today().moveToDayOfWeek(c.day_of_week, 1).toString(zomg.date_format);
            $('#task-date').attr('value', date);
          }
        });
      }
    });
    zomg.show_popup();
  },

  'new': function() {
    o_O.render('tasks/new', { priority: 3 }, {set: 'div#popup-contents',
           callback: TasksController._setup_form}, ['tasks/_form']);
  },

  create: function() {
    var task = Task.initialize(o_O.params($(this)));
    task.commit({
      invalid: function() {
        zomg.show_errors('div#popup form', task.errors);
      },
      success: function() {
        zomg.hide_popup();
        zomg.refresh();
      }
    });
  },

  toggle_complete: function(params) {
    var task = Task.find(params['id']);
    if(task.complete) {
      task.complete = false;
    } else {
      if(task.repeat && task.repeat != '') {
        var next;
        if(task.repeat == "week") {
          next = function(n) { return n.addWeeks(1); };
        } else if(task.repeat == "month") {
          next = function(n) { return n.addMonths(1); };
        } else if(task.repeat == "year") {
          next = function(n) { return n.addYears(1); };
        }

        var cur = Date.parse(task.date);
        if(cur.compareTo(next(Date.today())) <= 0)
          task.date = next(cur).toString(zomg.date_format);
      } else {
        task.complete = true;
      }
    }
    task.commit();

    zomg.refresh();
  },

  edit: function(params) {
    var task = Task.find(params['id']);
    o_O.render('tasks/edit', task, {set: 'div#popup-contents',
        callback: TasksController._setup_form}, ['tasks/_form']);
  },

  update: function(params) {
    var task = Task.find(params['id']);
    task.update_attributes(o_O.params($(this)));
    task.commit({
      invalid: function() {
        zomg.show_errors('div#popup form', task.errors);
      },
      success: function() {
        zomg.hide_popup();
        zomg.refresh();
      }
    });
  },

  toggle_delete: function(params) {
    var task = Task.find(params['id']);
    task.deleted = !task.deleted;
    task.commit();

    zomg.refresh();
  },
});
