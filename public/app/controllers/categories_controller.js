/*
    Zomganizer: an HTML5 organizer with offline-mode and other features
    Copyright (C) 2010  Peter Zotov <whitequark@whitequark.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

o_O('CategoriesController', {
  'new': function() {
    o_O.render('categories/new', {}, {set: 'div#popup-contents',
           callback: zomg.show_popup}, ['categories/_form']);
  },

  create: function() {
    var category = Category.initialize(o_O.params($(this)));
    category.commit({
      invalid: function() {
        zomg.show_errors('div#popup form', category.errors);
      },
      success: function() {
        zomg.hide_popup();
        o_O.routes.redirect_to('settings#show');
      }
    });
  },

  edit: function(params) {
    var category = Category.find(params['id']);
    o_O.render('categories/edit', category, {set: 'div#popup-contents',
        callback: zomg.show_popup}, ['categories/_form']);
  },

  update: function(params) {
    var category = Category.find(params['id']);
    category.update_attributes(o_O.params($(this)));
    category.commit({
      invalid: function() {
        zomg.show_errors('div#popup form', category.errors);
      },
      success: function() {
        zomg.hide_popup();
        zomg.refresh();
      }
    });
  },

  destroy: function(params) {
    var category = Category.find(params['id']);
    category.deleted = true;
    category.commit();

    zomg.refresh();
  }
});
