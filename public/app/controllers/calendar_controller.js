/*
    Zomganizer: an HTML5 organizer with offline-mode and other features
    Copyright (C) 2010  Peter Zotov <whitequark@whitequark.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

o_O('CalendarController', {
  show: function(params, is_refresh) {
    if(params)
      params = params.call();
    else
      params = { year: new Date().getFullYear(), month: new Date().getMonth()+1 };

    if(is_refresh)
      params = CalendarController.old_params;

    CalendarController.old_params = params;

    o_O.render('calendar/show', {}, {set: 'div#yield',
      callback: function() {
        var target = Date.today().set({ year: parseInt(params.year), month: parseInt(params.month)-1 });
        var start = target.clone().moveToFirstDayOfMonth();
        if(start.getDay() != 1) start.moveToDayOfWeek(1, -1);
        var end = target.clone().moveToLastDayOfMonth();
        if(end.getDay() != 0) end.moveToDayOfWeek(0, +1);
        var current = start.clone();

        var tasks = Task.all().map(Task.filter).compact().map(Task.prepare);

        var div = $('.calendar');

        var prev = target.clone().addMonths(-1);
        var next = target.clone().addMonths(1);
        var nav = '<nav class="options">'
        nav += '<a href="#calendar/'+prev.getFullYear()+'/'+(prev.getMonth()+1)+'">&laquo; Previous</a>';
        nav += ' <span class="current">'+target.toString('MMMM yyyy')+'</span> '
        nav += '<a href="#calendar/'+next.getFullYear()+'/'+(next.getMonth()+1)+'">Next &raquo;</a></nav>';
        nav += '</nav>';
        div.append(nav);

        var subdiv = '';
        while(current <= end) {
          if(current.getDay() == 1) { // monday
            if(subdiv != '')
              subdiv += '</div>';

            div.append(subdiv);
            subdiv = '<div class="week">';
          }

          var type = '';
          if(current.getMonth() != target.getMonth())
            type += ' other';
          if(current - Date.today() == 0)
            type += ' today';

          subdiv += '<div class="day'+type+'"><span class="number">' +
                    current.toString('MMM d') + '</span><div class="window">';
          $A(tasks).each(function(task) {
            if(Date.parse(task.date) - current == 0) {
              subdiv += '<span class="task prio-'+task.priority+'">';
              subdiv += '<span class="show action" data-id="'+task.id+'" data-bind="tasks#show"></span>';
              if(task.category != '')
                subdiv += task.category + ' ';
              subdiv += task.name;
              subdiv += '</span>';
            }
          });
          subdiv += '</div></div>';

          current.add({ day: 1 });
        }
        div.append(subdiv);
      }
    });

    zomg.current_view = CalendarController.show;
  }
});