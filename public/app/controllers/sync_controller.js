/*
    Zomganizer: an HTML5 organizer with offline-mode and other features
    Copyright (C) 2010  Peter Zotov <whitequark@whitequark.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

o_O('SyncController', {
  datasets: [ Task, Category ],

  initial: function() {
    if(!localStorage['zomg']) {
      zomg.show_popup(true);
      SyncController.start(); // initial sync
    } else {
      TasksController.dashboard();
    }
  },

  start: function() {
    if(!zomg.authNonce) {
      o_O.render('sync/login', {}, {set:'div#popup-contents',
        callback: function() {
          $('#login').attr('value', localStorage['login']);
          $('#password').attr('value', localStorage['password']);
          zomg.show_popup();
        }
      });
    } else {
      SyncController.sync();
    }
  },

  _display_status: function(status) {
    o_O.render('sync/sync', {status: status}, {set:'div#popup-contents', callback: zomg.show_popup});
  },

  _display_error: function(error) {
    o_O.render('sync/error', {error: error}, {set:'div#popup-contents', callback: zomg.show_popup});
  },

  auth: function() {
    var params = o_O.params($(this));
    if(params['remember']) {
      localStorage['login'] = params.login;
      localStorage['password'] = params.password;
    }

    SyncController._display_status('requesting token');
    $.ajax({ url: 'token', dataType: 'json', success: function(data) {
      var digestNonce = data['token'];
      SyncController._display_status('authenticating');

      var digest = sha1(digestNonce + sha1(params.login) + sha1(params.password));
      $.ajax({ url: 'auth/'+digest, dataType: 'json', success: function(data) {
        zomg.authNonce = data['token'];
        SyncController.sync();
      }, error: function() {
        SyncController._display_error('authentication failure');
      }});
    }, error: function() {
      SyncController._display_error('(probably) network failure');
    }});
  },

  sync: function(params) {
    SyncController._display_status('synchronizing');

    function dissect(object, attrs) {
      var disobj = {};
      for(var attr in object) {
        if($A(attrs).include(attr))
          disobj[attr] = object[attr];
      }
      return disobj;
    }

    var data = {};
    $A(SyncController.datasets).each(function(ds) {
      var name = ds.model_name;
      data[name] = { store: {}, drop: $A([]) };

      $A(ds.all()).each(function(obj) {
        if(obj.deleted) {
          data[name].drop.push(obj.id);
        }
        if(!obj.synced && !obj.deleted) {
          data[name].store[obj.id] = dissect(obj, ds.persistent);
        }
      });
    });

    $.ajax({ url: 'sync/'+zomg.authNonce, dataType: 'json', type: 'POST',
             data: JSON.stringify(data), success: function(data) {
      $H(data).each(function(pair) {
        var ds = self[pair.key], branch = pair.value;
        $A(branch['drop']).each(function(id) {
          ds.find(id, {
            success: function(obj) {
              obj.destroy();
            }
          });
        });
        $H(branch['store']).each(function(pair) {
          var id = pair.key, value = pair.value;
          ds.find(id, {
            success: function(obj) {
              obj.update_attributes(value);
              obj.synced = true;
              obj.deleted = false;
              obj.save();
            },
            failure: function() {
              var obj = ds.initialize(value);
              obj.id = id;
              obj.synced = true;
              obj.save();
            }
          });
        });
      });

      zomg.check_sync();
      zomg.hide_popup();

      if(!localStorage['zomg']) {
        localStorage['zomg'] = true;
        o_O.routes.redirect_to('tasks#dashboard');
      } else {
        zomg.refresh();
      }
    }, failure: function() {
      SyncController._display_error('synchronization failure');
    }});
  },

  resync: function() {
    $A(SyncController.datasets).each(function(ds) {
      $A(ds.all()).each(function(obj) {
        obj.synced = false;
        obj.save();
      })
    });
    zomg.check_sync();
    SyncController.start();
  },

  try_logout: function() {
    if(zomg.check_sync()) {
      o_O.render('sync/desynced', {}, {set:'div#popup-contents', callback: zomg.show_popup});
    } else {
      SyncController.logout();
    }
  },

  sync_then_logout: function() {
    zomg.current_view = SyncController.logout;
    SyncController.start();
  },

  logout: function() {
    localStorage.clear();
    $('div#yield').remove();

    zomg.show_popup(true);
    o_O.render('sync/logout', {}, {set:'div#popup-contents' });
  }
});
