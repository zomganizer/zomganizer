/*
    Zomganizer: an HTML5 organizer with offline-mode and other features
    Copyright (C) 2010  Peter Zotov <whitequark@whitequark.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Inspired by http://www.iana.org/assignments/_support/sort.js */

// Stable sort. Time is O(n*log(n)). Storage is O(2*n). Non-recursive because
// browsers impose a fairly low recursion limit.
function mergeSort(rows, sortCompare)
{
  for(var s = 2; ; s *= 2) {
    for(var begin = 0; begin < rows.length; begin += s) {
      var middle = (begin + begin + s) / 2;
      var end = Math.min(begin + s, rows.length);
      var left  = rows.slice(begin, middle);
      var right = rows.slice(middle, end);
      var i = begin;

      while(left.length > 0 && right.length > 0) {
        if(sortCompare(left[0], right[0]) <= 0)
          rows[i++] = left.shift();
        else
          rows[i++] = right.shift();
      }

      while(left.length > 0)
        rows[i++] = left.shift();
      while(right.length > 0)
        rows[i++] = right.shift();
    }

    if(s >= rows.length)
      break;
  }
};

function sortList(list, column) {
  function reorderRows(list, callback) {
    var tempDiv = document.createElement('div');
    var rowCount = list.children.length-1;
    for(var i = 0; i < rowCount; i++)
      tempDiv.appendChild(callback(i));
    for(var i = 0; i < rowCount; i++)
      list.appendChild(tempDiv.children[0]);
  }

  var sortModes = {
    text: {
      transform: function(text) {
        return text.trim().toLowerCase();
      },
      compare: function(a, b) {
        return a[0].localeCompare(b[0]);
      }
    },

    numeric: {
      transform: function(text) {
        var parts = text.split( /[^\d]+/);
        for(var i = 0; i < parts.length; i++)
          parts[i] = parseInt(parts[i], 10);
        return parts;
      },
      compare: function(a, b) {
        for(var i = 0; i < a[0].length && i < b[0].length; i++) {
          if(a[0][i] < b[0][i]) return -1;
          if(a[0][i] > b[0][i]) return +1;
        }
        if(a[0].length < b[0].length) return -1;
        if(a[0].length > b[0].length) return +1;
        return 0;
      }
    },

    date: {
      transform: function(text) {
        return Date.parse(text);
      },
      compare: function(a, b) {
        return a[0].compareTo(b[0]);
      }
    }
  };

  var headers = list.children[0].children;
  var header = headers[column];

  var sortMode = sortModes.text;
  for(var modeName in sortModes) {
    if($(header).hasClass('sort-' + modeName))
      sortMode = sortModes[modeName];
  }

  function worker() {
    if($(header).hasClass('sorted')) {
      $(header).toggleClass('reversed');
      reorderRows(list, function(i) { return list.children[list.children.length-1]; });
    } else {
      var rows = [];
      for(var i = 1; i < list.children.length; i++) {
        var cell = list.children[i].children[column];
        rows.push([ sortMode.transform(cell.textContent), list.children[i] ]);
      }

      mergeSort(rows, sortMode.compare);
      reorderRows(list, function(i) { return rows[i][1]; });

      for(var i = 0; i < headers.length; i++) {
        $(headers[i]).removeClass('sorted');
        $(headers[i]).removeClass('reversed');
      }

      $(header).addClass('sorted');
    }
  }

  setTimeout(worker, 0); // sort in background
}

function sortListByColumn(column) {
  var list = $(column).parents('div.list')[0];

  var headers = list.children[0].children;
  var column;
  for(var i = 0; i < headers.length; i++) {
    if(headers[i] == column) {
      column = i;
      break;
    }
  }

  sortList(list, column);
}

$('div.list div.header span:not(.action)').live('click', function(event) {
  sortListByColumn(event.srcElement);
});

function sortListsInitially() {
  $('.sort-initial').each(function() {
    sortListByColumn(this);
  });
}

