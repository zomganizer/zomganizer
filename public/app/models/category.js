/*
    Zomganizer: an HTML5 organizer with offline-mode and other features
    Copyright (C) 2010  Peter Zotov <whitequark@whitequark.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

o_O('Category', function(category) {
  category.validates_presence_of('name');

  category.methods.commit = function(options) {
    this.synced = false;
    this.save(options);

    zomg.check_sync();
  }

  category.prepare = function(c) {
    c.options_text = "";

    if(c.day_of_week > 0) {
      c.options_text += "Repeats each " + Date.today().moveToDayOfWeek(c.day_of_week).toString("dddd") + ".";
    }

    return c;
  };

  category.filter = function(c) {
    if(c.deleted)
      return null;
    return c;
  };

  category.persistent = [ 'name', 'day_of_week' ];
});

