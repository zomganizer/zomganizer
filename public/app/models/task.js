/*
    Zomganizer: an HTML5 organizer with offline-mode and other features
    Copyright (C) 2010  Peter Zotov <whitequark@whitequark.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

o_O('Task', function(task) {
  task.validates_presence_of('description');
  task.validates_presence_of('date');

  task.methods.commit = function(options) {
    this.synced = false;
    this.save(options);

    zomg.check_sync();
  }

  task.prepare = function(t) {
    var lines = t.description.split("\n");
    t.name = lines[0];
    t.brief = lines.splice(1, lines.length).join("\n");
    if(t.name.length + t.brief.length > 80)
      t.brief_short = t.brief.substr(0, 80 - t.name.length).replace("\n", ' ') + '...';
    if(t.complete)
      t.status = 'complete';
    else
      t.status = 'incomplete';
    if(t.deleted)
      t.status = 'deleted';
    if(t.repeat && t.repeat != '')
      t.status += ' repeat';

    try {
      var pdate = Date.parse(t.date);
      var today = Date.today();
      if(pdate - today < 0) {
        t.schedule = 'missed';
        if(!t.complete)
          t.status += ' missed';
      } else if(pdate - today == 0) {
        t.schedule = 'today';
      } else if(pdate - today <= 86400000) {
        t.schedule = 'tomorrow';
      } else if(pdate - today <= 86400000*7) {
        t.schedule = 'week';
      }
    } catch(e) {
    }

    return t;
  };

  task.filter = function(t) {
    if(t.deleted || t.complete)
      return null;
    return t;
  };

  task.persistent = [ 'description', 'priority', 'date', 'time', 'category',
      'location', 'complete', 'repeat' ];
});
