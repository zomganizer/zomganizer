o_O.routes.draw(function(map){
  map.root({to: 'sync#initial'});

  map.match('/sync',       {to: 'sync#start'});
  map.match('/logout',     {to: 'sync#try_logout'});

  map.match('/dashboard',  {to: 'tasks#dashboard'});

  map.match('/calendar/:year/:month',   {to: 'calendar#show'});
  map.match('/calendar',                {to: 'calendar#show'});

  map.match('/task/list',        {to: 'tasks#list'});
  map.match('/task/new',         {to: 'tasks#new'});
  map.match('/task/edit/:id',    {to: 'tasks#edit'});

  map.match('/category/new',      {to: 'categories#new'});
  map.match('/category/edit/:id', {to: 'categories#edit'});

  map.match('/settings', {to: 'settings#show'});
})
