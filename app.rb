=begin
    Zomganizer: an HTML5 organizer with offline-mode and other features
    Copyright (C) 2010  Peter Zotov <whitequark@whitequark.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end

# requires JSON *GEM*, because Ruby 1.9.0 (at least) packer kills Unicode.

require 'rubygems'
require 'sinatra'
require 'sass'
require 'yaml'
require 'json'
require 'digest/sha1'

set :static, true
set :views, Proc.new { File.expand_path('.') }
set :sass, { :syntax => :scss }

def asset(name, mode=nil)
  File.open(File.join(settings.root, name), mode) { |f|
    return yield f
  }
end

def make_nonce
  40.times.map { ((?0..?9).to_a + (?a..?f).to_a)[rand(16)].chr }.join
end

$cache_nonce = make_nonce

get '/' do
  content_type 'text/html', :charset => 'utf-8'
  asset('index.html') { |f| f.read }
end

get '/style.css' do
  content_type 'text/css', :charset => 'utf-8'
  sass :stylesheet
end

get '/cache.manifest' do
  halt 404 if Sinatra::Application.environment == :development

  content_type 'text/cache-manifest'
  public_root = File.join settings.root, "public"
  @cache = Dir[File.join(public_root, "**", "*")].
       select { |d| File.file?(d) }
  @rev = Digest::SHA1.hexdigest((@cache + %w{index.html stylesheet.sass}.
          map {|s| File.join(settings.root, s) }).
       map { |f| "#{f}#{File.mtime(f)}" }.join)

  @cache.map! { |d| d.sub("#{public_root}/", '') }
  erb :cache_manifest
end

digest_tokens = []
auth_tokens = []

get '/token' do
  content_type 'text/json', :charset => 'utf-8'

  dtoken = make_nonce
  digest_tokens << dtoken
  "{ \"token\": \"#{dtoken}\" }"
end

get '/auth/:digest' do
  content_type 'text/json', :charset => 'utf-8'

  _digest = lambda { |s| Digest::SHA1.hexdigest(s) }

  config = asset('config.yml') { |f| YAML.load(f.read) }
  cred_digest = _digest[config['login']] + _digest[config['password']]

  if digest_tokens.select { |dtoken| params[:digest] == _digest[dtoken + cred_digest] }.any?
    atoken = make_nonce
    auth_tokens << atoken
    "{ \"token\": \"#{atoken}\" }"
  else
    halt 403
  end
end

post '/sync/:auth' do
  content_type 'text/json', :charset => 'utf-8'

  halt 401 if !auth_tokens.include? params[:auth]

  data = JSON.parse(request.env["rack.input"].read)
  stored_data = asset('data.json') { |f| JSON.parse(f.read) || {} } rescue {}

  data.each { |klass, branch|
    stored_branch = (stored_data[klass] ||= { 'store' => {}, 'drop' => [] })
    JSON.parse(branch['drop']).each { |id|
      stored_branch['store'].delete id
      stored_branch['drop'] << id
      stored_branch['drop'].uniq!
    }
    branch['store'].each { |id, value|
      stored_branch['drop'].delete id
      stored_branch['store'][id] = value
    }
  }

  stored_json = JSON.dump(stored_data)

  asset('data.json', 'w') { |f| f.write stored_json }

  stored_json
end

__END__

@@ cache_manifest
CACHE MANIFEST
NETWORK:
token
auth
sync
CACHE:
# rev <%= @rev %> nonce <%= $cache_nonce %>
style.css
<%= @cache.join "\n" %>
